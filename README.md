# Restaurants list

Basic Android native application displaying a list of restaurants from storage.googleapis

- It uses <https://storage.googleapis.com/coding-session-rest-api/restaurants> URL
to get the restaurants list

To make it running just import it to Android Studio v3.5.3 or
use ./gradlew assemble command to assemble the apk and install it on a device

![](./art/Screenshot_1.png)

TODO:
- Use MockWebServer or similar to mock network connection in the
UI/instrumentation tests and write more tests
- add static code analysis tool (deteKt)
- more testing...
package uk.co.tomek.restaurants.data.net

import retrofit2.http.GET
import uk.co.tomek.restaurants.data.model.RestaurantsResponse

interface RestaurantsService {

    @GET("coding-session-rest-api/restaurants")
    suspend fun getRestaurants(): RestaurantsResponse

}
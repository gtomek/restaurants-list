package uk.co.tomek.restaurants.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.item_restaurants_list.view.*
import timber.log.Timber
import uk.co.tomek.restaurants.R
import uk.co.tomek.restaurants.presentation.model.RestaurantModel

class RestaurantsListAdapter(
    private val clickListener: (RestaurantModel) -> Unit
) : ListAdapter<RestaurantModel, RecyclerView.ViewHolder>(RestaurantsDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_restaurants_list, parent, false)
        return RestaurantItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RestaurantItemViewHolder).bind(getItem(position), clickListener)   }

    class RestaurantItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(
            model: RestaurantModel,
            clickListener: (RestaurantModel) -> Unit
        ) {
            Timber.v("Bind RestaurantItemViewHolder $model")
            with(itemView) {
                imageview_hero_image.load(model.imageUrl) {
                    crossfade(true)
                    placeholder(R.drawable.ic_insert_photo_black_24dp)
                }
                textview_title.text = model.title
                textview_address.text = model.address
                textview_categories.text = model.categoryDescription
                model.callActionUri?.let {
                    textview_action_button.apply {
                        isVisible = true
                        setOnClickListener { clickListener.invoke(model) }
                    }
                }
                imageview_red_icon.apply {
                    isVisible = true
                    load(model.iconUrl) {
                        crossfade(true)
                        placeholder(R.drawable.ic_restaurant_black_24dp)
                    }
                }
            }
        }
    }
}
package uk.co.tomek.restaurants.data.model

data class FeedbackDimension(
    val dimension: String,
    val label: String
)
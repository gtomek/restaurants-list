package uk.co.tomek.restaurants.data.model

data class Logo(
    val image: String,
    val url: String
)
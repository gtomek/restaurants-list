package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class PrimaryAction(
    @Json(name = "icon_url")
    val iconUrl: String,
    val label: String,
    val type: String,
    val uri: String
)
package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class Contact(
    @Json(name = "contact_value")
    val contactValue: String,
    val display: String,
    val label: String,
    @Json(name = "refuse_advertising")
    val refuseAdvertising: Boolean,
    val type: String
)
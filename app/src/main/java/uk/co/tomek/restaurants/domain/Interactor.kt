package uk.co.tomek.restaurants.domain

/**
 * Interactor/Use case abstraction used to interact between data and presentation layers
 */
interface Interactor<T> {
    suspend fun fetchData(pageNumber: Int): T
}
package uk.co.tomek.restaurants.data.model

data class Category(
    val id: String,
    val name: String
)
package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class Restaurant(
    val actions: List<Action>?,
    @Json(name = "can_manage")
    val canManage: Boolean?,
    val categories: List<Category>,
    @Json(name = "contact_groups")
    val contactGroups: List<ContactGroup>,
    val context: String,
    val description: String,
    @Json(name = "entry_type")
    val entryType: String,
    @Json(name = "feedback_dimensions")
    val feedbackDimensions: List<FeedbackDimension>,
    @Json(name = "feedback_summary")
    val feedbackSummary: FeedbackSummary?,
    @Json(name = "followers_count")
    val followersCount: Int,
    val following: Boolean,
    val icons: Icons,
    val id: String,
    val images: Images,
    @Json(name = "local_business_type")
    val localBusinessType: String,
    val location: Location,
    @Json(name = "opening_hours")
    val openingHours: OpeningHours,
    @Json(name = "primary_action")
    val primaryAction: PrimaryAction,
    @Json(name = "row_type")
    val rowType: String,
    val subtitle: String,
    val title: String,
    @Json(name = "web_permalink")
    val webPermalink: String
)


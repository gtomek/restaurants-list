package uk.co.tomek.restaurants.data.model

data class DetailEntry(
    val src: String,
    val tint: Boolean,
    val type: String
)
package uk.co.tomek.restaurants.presentation

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_error.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uk.co.tomek.restaurants.R
import uk.co.tomek.restaurants.presentation.model.MainViewState
import uk.co.tomek.restaurants.presentation.model.RestaurantModel
import uk.co.tomek.restaurants.presentation.viewmodel.MainViewModel

/**
 * Displays main application screen.
 */
class MainActivity : AppCompatActivity() {

    private lateinit var restaurantsListAdapter: RestaurantsListAdapter
    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        restaurantsListAdapter = RestaurantsListAdapter(::makeCall)

        recycler_items_list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = restaurantsListAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                    if (dy > 0) { // scrolling down
                        val lastVisible = layoutManager.findLastCompletelyVisibleItemPosition()
                        if (layoutManager.itemCount > 0 && lastVisible == layoutManager.itemCount - 1) {
                            mainViewModel.onBottomReached()
                        }
                    }
                }
            })
        }

        mainViewModel.mainViewState.observe(this, Observer { viewState ->
            viewState?.let { renderState(it) }
        })

        button_error_layout_try_again.setOnClickListener {
            mainViewModel.retryClicked()
            Toast.makeText(this,"Retry clicked", Toast.LENGTH_LONG).show()
        }
    }

    private fun renderState(state: MainViewState) {
        Timber.v("Render view state $state")
        when (state) {
            is MainViewState.Loading -> {
                recycler_items_list.visibility = View.GONE
                layout_error_main.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE
            }
            is MainViewState.Data -> {
                recycler_items_list.visibility = View.VISIBLE
                progress_bar.visibility = View.GONE
                layout_error_main.visibility = View.GONE
                state.itemsResponse.let {
                    restaurantsListAdapter.submitList(it)
                }

                if (state.itemsResponse.isEmpty()) {
                    //TODO:DisplayEmptyState
                }
            }
            is MainViewState.Error -> {
                recycler_items_list.visibility = View.GONE
                progress_bar.visibility = View.GONE
                layout_error_main.visibility = View.VISIBLE
                Timber.e(state.throwable)
            }
        }
    }

    private fun makeCall(model: RestaurantModel) {
        model.callActionUri?.let {
            dialPhoneNumber(it)
        }
    }

    private fun dialPhoneNumber(phoneNumberUri: String) {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse(phoneNumberUri)
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

}

package uk.co.tomek.restaurants.domain

import uk.co.tomek.restaurants.data.model.RestaurantsResponse
import uk.co.tomek.restaurants.presentation.model.RestaurantModel

/**
 * Basic mapper that converting restaurants data between the data layer and UI
 */
class RestaurantsDataMapper() {

    fun mapPageToViewData(response: RestaurantsResponse): List<RestaurantModel> =

        response.results.map { result ->
            val callUri = if (result.primaryAction.type == CALL) {
                result.primaryAction.uri
            } else {
                null
            }
            RestaurantModel(
                title = result.title,
                address = result.subtitle,
                categoryDescription = result.categories.joinToString(separator = " • ") { it.name },
                iconUrl = result.icons.listBadge,
                imageUrl = result.icons.resultList.src,
                callActionUri = callUri
            )
        }

    companion object {
        const val CALL = "call"
    }
}
package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class Stats(
    @Json(name = "items_per_page")
    val itemsPerPage: Int,
    @Json(name = "start_index")
    val startIndex: Int,
    @Json(name = "total_results")
    val totalResults: Int
)
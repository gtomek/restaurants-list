package uk.co.tomek.restaurants.data

import uk.co.tomek.restaurants.data.model.RestaurantsResponse
import uk.co.tomek.restaurants.data.net.RestaurantsService

class RestaurantsRepository(private val networkSource: RestaurantsService) :
    Repository<RestaurantsResponse> {

    override suspend fun fetchData(pageNumber: Int): RestaurantsResponse =
        networkSource.getRestaurants()

}
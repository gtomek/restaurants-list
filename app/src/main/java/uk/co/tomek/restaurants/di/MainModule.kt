package uk.co.tomek.restaurants.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import uk.co.tomek.restaurants.BuildConfig
import uk.co.tomek.restaurants.data.Repository
import uk.co.tomek.restaurants.data.RestaurantsRepository
import uk.co.tomek.restaurants.data.model.RestaurantsResponse
import uk.co.tomek.restaurants.data.net.RestaurantsService
import uk.co.tomek.restaurants.domain.Interactor
import uk.co.tomek.restaurants.domain.MainInteractor
import uk.co.tomek.restaurants.domain.RestaurantsDataMapper
import uk.co.tomek.restaurants.presentation.model.MainViewState
import uk.co.tomek.restaurants.presentation.viewmodel.MainViewModel

/**
 * KOIN modules declarations.
 */
val applicationModule: Module = module {

    factory<Repository<RestaurantsResponse>> {
        RestaurantsRepository(
            get()
        )
    }

    factory<Interactor<MainViewState>> {
        MainInteractor(
            get(),
            get()
        )
    }
    single { RestaurantsDataMapper() }
    viewModel { MainViewModel(get()) }
}

val networkModule: Module = module {
    single { createOkHttpClient() }

    single {
        creteNetService<RestaurantsService>(
            get(),
            BuildConfig.GOOGLE_STORAGE_SERVER_URL
        )
    }
}

fun createOkHttpClient(): OkHttpClient {
    val logInterceptor = HttpLoggingInterceptor()
    logInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return OkHttpClient.Builder()
        .addInterceptor(logInterceptor)
        .build()
}

inline fun <reified T> creteNetService(httpClient: OkHttpClient, baseUrl: String): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(httpClient)
        .addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder()
                    .add(KotlinJsonAdapterFactory())
                    .build()
            )
        )
        .build()
    return retrofit.create(T::class.java)
}
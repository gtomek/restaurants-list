package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class Address(
    val city: String,
    @Json(name = "house_number")
    val houseNumber: String,
    @Json(name = "precomposed_lines")
    val precomposedLines: List<String>,
    val state: String,
    val street: String,
    val zipcode: String
)
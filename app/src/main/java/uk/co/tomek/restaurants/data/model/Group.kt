package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class Group(
    val days: String,
    @Json(name = "is_closed")
    val isClosed: Boolean?,
    @Json(name = "is_open")
    val isOpen: Boolean?,
    val times: List<String>?,
    val today: Boolean?
)
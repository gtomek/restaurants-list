package uk.co.tomek.restaurants.presentation.model

/**
 * Models representing the view item on a restaurant list.
 */
data class RestaurantModel(
    val title: String,
    val address: String,
    val categoryDescription: String,
    val imageUrl: String,
    val iconUrl: String,
    val callActionUri: String?
)
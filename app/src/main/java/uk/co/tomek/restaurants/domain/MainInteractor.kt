package uk.co.tomek.restaurants.domain

import uk.co.tomek.restaurants.data.Repository
import uk.co.tomek.restaurants.data.model.RestaurantsResponse
import uk.co.tomek.restaurants.presentation.model.MainViewState

/**
 * Main interactor implementation.
 */
class MainInteractor(
    private val repository: Repository<RestaurantsResponse>,
    private val restaurantMapper: RestaurantsDataMapper
) : Interactor<MainViewState> {

    override suspend fun fetchData(pageNumber: Int): MainViewState {
        return try {
            val response = repository.fetchData()
            MainViewState.Data(
                restaurantMapper.mapPageToViewData(response),
                totalResults = response.stats.totalResults,
                totalPages = response.stats.totalResults / response.stats.itemsPerPage
            )
        } catch (exception: Exception) {
            MainViewState.Error(exception)
        }
    }

}
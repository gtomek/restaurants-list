package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class FeedbackSummary(
    @Json(name = "average_rating")
    val averageRating: Float,
    @Json(name = "detail_url")
    val detailUrl: String,
    @Json(name = "positive_recommendation_percentage")
    val positiveRecommendationPercentage: Int,
    @Json(name = "positive_recommendations")
    val positiveRecommendations: Int,
    @Json(name = "rating_summaries")
    val ratingSummaries: List<RatingSummary>,
    @Json(name = "ratings_count")
    val ratingsCount: Int,
    val recommendations: Int
)
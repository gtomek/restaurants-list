package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class Location(
    val address: Address,
    val coords: Coords,
    @Json(name = "static_map_url")
    val staticMapUrl: String
)
package uk.co.tomek.restaurants.data.model

data class Coords(
    val latitude: Double,
    val longitude: Double
)
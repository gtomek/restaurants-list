package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class Icons(
    @Json(name = "detail_badge")
    val detailBadge: String,
    @Json(name = "detail_entry")
    val detailEntry: DetailEntry,
    @Json(name = "list_badge")
    val listBadge: String,
    @Json(name = "result_list")
    val resultList: ResultList
)
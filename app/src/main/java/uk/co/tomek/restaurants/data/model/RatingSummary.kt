package uk.co.tomek.restaurants.data.model

data class RatingSummary(
    val average: Float,
    val count: Int,
    val dimension: String,
    val label: String
)
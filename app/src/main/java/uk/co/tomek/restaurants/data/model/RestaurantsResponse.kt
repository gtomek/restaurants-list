package uk.co.tomek.restaurants.data.model

data class RestaurantsResponse(
    val stats: Stats,
    val results: List<Restaurant>
)
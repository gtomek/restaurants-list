package uk.co.tomek.restaurants.presentation

import androidx.recyclerview.widget.DiffUtil
import uk.co.tomek.restaurants.presentation.model.RestaurantModel

/**
 * Simplified version of diff utils for the restaurants list.
 */
class RestaurantsDiffUtil : DiffUtil.ItemCallback<RestaurantModel>() {

    override fun areContentsTheSame(oldItem: RestaurantModel, newItem: RestaurantModel): Boolean =
        oldItem.title == newItem.title


    override fun areItemsTheSame(oldItem: RestaurantModel, newItem: RestaurantModel): Boolean =
        oldItem == newItem
}
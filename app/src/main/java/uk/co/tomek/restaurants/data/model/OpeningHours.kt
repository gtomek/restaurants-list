package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class OpeningHours(
    val groups: List<Group>,
    @Json(name = "is_now_open")
    val isNowOpen: Boolean
)
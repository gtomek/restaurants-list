package uk.co.tomek.restaurants.data.model

data class ContactGroup(
    val contacts: List<Contact>,
    val header: String?
)
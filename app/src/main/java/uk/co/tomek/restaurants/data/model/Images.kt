package uk.co.tomek.restaurants.data.model

import com.squareup.moshi.Json

data class Images(
    @Json(name = "background_image")
    val backgroundImage: String,
    val gallery: List<String>,
    val logo: Logo
)
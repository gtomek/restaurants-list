package uk.co.tomek.restaurants.domain

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import uk.co.tomek.restaurants.data.model.*
import uk.co.tomek.restaurants.presentation.model.RestaurantModel

class RestaurantsDataMapperTest {


    private lateinit var mapper: RestaurantsDataMapper

    @Before
    fun setUp() {
        mapper = RestaurantsDataMapper()
    }

    @Test
    fun verifyNormalMapping() {
        // given
        val title1 = "title1"
        val subtitle1 = "subtitle"
        val category1 = "category1"
        val category2 = "category2"
        val category3 = "category3"
        val categories1 = listOf(
            Category("1", category1),
            Category("2", category2),
            Category("3", category3))
        val myUri = "myUri"
        val action = PrimaryAction("url", "label", "call", myUri)
        val listBadge1 = "lsitBadge"
        val resultListt = "resultListt"
        val resultlist1 = ResultList(resultListt, false, "kuku")
        val icons1 = mock<Icons> {
            on {listBadge} doReturn listBadge1
            on {resultList} doReturn resultlist1
        }
        val restaurant1 = mock<Restaurant> {
            on { primaryAction } doReturn action
            on { title } doReturn title1
            on { subtitle } doReturn subtitle1
            on { categories } doReturn categories1
            on { icons } doReturn icons1
        }
        val resultsList: List<Restaurant> = listOf(restaurant1)
        val restaurantsResponse = RestaurantsResponse(
            Stats(10, 0, 100),
            results = resultsList
        )
        val expectedModel = RestaurantModel(
            title = title1,
            address = subtitle1,
            categoryDescription = "category1 • category2 • category3",
            imageUrl = resultListt,
            iconUrl = listBadge1,
            callActionUri = myUri
        )
        val expected = listOf(expectedModel)

        //when
        val mappedData = mapper.mapPageToViewData(restaurantsResponse)

        // then
        assertEquals(expected, mappedData)

    }

}
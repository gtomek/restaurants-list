package uk.co.tomek.restaurants.domain

import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import uk.co.tomek.restaurants.data.Repository
import uk.co.tomek.restaurants.data.model.*
import uk.co.tomek.restaurants.presentation.model.MainViewState
import uk.co.tomek.restaurants.presentation.model.RestaurantModel

class MainInteractorTest {

    @Mock
    private lateinit var repository: Repository<RestaurantsResponse>

    private lateinit var interactor: MainInteractor

    private val mapper = RestaurantsDataMapper()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        interactor = MainInteractor(repository, mapper)
    }

    @Test
    fun verifyAnErrorIsPropagatedWhenConnectionFails() {
        // given
        val expection = RuntimeException("an expection")
        val expected = MainViewState.Error(expection)

        // when
        runBlocking { whenever(repository.fetchData(any())) }.thenThrow(expection)
        val result = runBlocking { interactor.fetchData(1) }

        // then
        assertEquals(expected, result)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun verifyThatDataStateIsReturnedInCaseOfNormalDataFetch() {

        runBlockingTest {
            // given

            val title1 = "title1"
            val subtitle1 = "subtitle"
            val category1 = "category1"
            val category2 = "category2"
            val category3 = "category3"
            val categories1 = listOf(
                Category("1", category1),
                Category("2", category2),
                Category("3", category3))
            val myUri = "myUri"
            val action = PrimaryAction("url", "label", "call", myUri)
            val listBadge1 = "lsitBadge"
            val resultListt = "resultListt"
            val resultlist1 = ResultList(resultListt, false, "kuku")
            val icons1 = mock<Icons> {
                on {listBadge} doReturn listBadge1
                on {resultList} doReturn resultlist1
            }
            val restaurant1 = mock<Restaurant> {
                on { primaryAction } doReturn action
                on { title } doReturn title1
                on { subtitle } doReturn subtitle1
                on { categories } doReturn categories1
                on { icons } doReturn icons1
            }
            val resultsList: List<Restaurant> = listOf(restaurant1)
            val restaurantsResponse = RestaurantsResponse(
                Stats(10, 0, 100),
                results = resultsList
            )

            val expectedModel = RestaurantModel(
                title = title1,
                address = subtitle1,
                categoryDescription = "category1 • category2 • category3",
                imageUrl = resultListt,
                iconUrl = listBadge1,
                callActionUri = myUri
            )
            given(repository.fetchData(any())).willReturn(restaurantsResponse)
            val expectedState = MainViewState.Data(
                listOf(expectedModel),
                totalPages = 10,
                totalResults = 100
            )

            // when
            val result = interactor.fetchData(1)

            // then
            assertEquals(expectedState, result)
        }

    }
}
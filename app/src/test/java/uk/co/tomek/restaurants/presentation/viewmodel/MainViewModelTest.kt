package uk.co.tomek.restaurants.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import uk.co.tomek.restaurants.CoroutinesMainTestRule
import uk.co.tomek.restaurants.captureValues
import uk.co.tomek.restaurants.domain.Interactor
import uk.co.tomek.restaurants.getValueForTest
import uk.co.tomek.restaurants.presentation.model.MainViewState

@ExperimentalCoroutinesApi
class MainViewModelTest {

    private val dispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()
    private lateinit var mainViewModel: MainViewModel

    @Mock
    private lateinit var interactor: Interactor<MainViewState>

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testRule = CoroutinesMainTestRule(dispatcher)

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mainViewModel = MainViewModel(interactor, dispatcher)
    }

    @Test
    fun verifyThatErrorIsPropagatedWhenLoadingFails() {
        runBlockingTest {
            // given
            val throwable = Exception("An exception")
            val errorState = MainViewState.Error(throwable)
            given(interactor.fetchData(any())).willReturn(errorState)

            // when
            mainViewModel.fetchRestaurants(1)

            // then
            mainViewModel.mainViewState.captureValues {
                assertEquals(errorState, mainViewModel.mainViewState.getValueForTest())
            }
        }
    }

    @Test
    fun verifyThatDataStateIsPropagated() {
        runBlockingTest {
            // given
            val dataState = MainViewState.Data(mock(), lastPage = 1)
            given(interactor.fetchData(any())).willReturn(dataState)

            // when
            mainViewModel.fetchRestaurants(1)

            // then
            mainViewModel.mainViewState.captureValues {
                assertEquals(dataState, mainViewModel.mainViewState.getValueForTest())
            }
        }
    }

    @Test
    fun verifyThatRetryIsWorking() {
        runBlockingTest {
            // given
            val dataState = MainViewState.Data(mock(), lastPage = 1)
            given(interactor.fetchData(any())).willReturn(dataState)

            // when
            mainViewModel.retryClicked()

            // then
            mainViewModel.mainViewState.captureValues {
                assertEquals(dataState, mainViewModel.mainViewState.getValueForTest())
            }
        }
    }

}